import io

import yaml

import text2py


text = """
Volume A
color red
number first
Chapter A1 
Chapter A2
Volume B
number second
Chapter B1 color red
Chapter B2 color orange
Chapter B3 color yellow
Volume C
color yellow
Chapter C1

Annex 1
Annex 2    
"""


def test_parser_1():
    template = """
- regexp: 'Volume (?P<name>\w+)'
  key: "{name}"
"""

    parser = text2py.Parser(yaml.load(template, yaml.SafeLoader))
    output = parser.parse(io.StringIO(text))
    assert output == {'A': {}, 'B': {}, 'C': {}}


def test_parser_2():
    template = """
- regexp: 'Volume (?P<volume>\w+)'
  key: "{volume}"
  value:
    - regexp: 'color (?P<color>\w+)'
      key: "color"
      value: "{color}"
    - regexp: 'number (?P<number>\w+)'
      key: "number"
      value: "{number}"
"""

    parser = text2py.Parser(yaml.load(template, yaml.SafeLoader))
    output = parser.parse(io.StringIO(text))
    assert output == {'A': {'color': 'red', 'number': 'first'},
                      'B': {'number': 'second'},
                      'C': {'color': 'yellow'}}


def test_parser_3():
    template = """
- regexp: 'Volume (?P<volume>\w+)'
  key: "{volume}"
  value:
    - regexp: '(?P<key>[a-z]+) (?P<value>\w+)'
      key: "{key}"
      value: "{value}"
"""

    parser = text2py.Parser(yaml.load(template, yaml.SafeLoader))
    output = parser.parse(io.StringIO(text))
    assert output == {'A': {'color': 'red', 'number': 'first'},
                      'B': {'number': 'second'},
                      'C': {'color': 'yellow'}}

def test_parser_4():
    template = """
- regexp: 'Volume (?P<volume>\w+)'
  key: "{volume}"
  value:
    - regexp: '(?P<key>[a-z]+) (?P<value>\w+)'
      key: "{key}"
      value: "{value}"
    - regexp: '(?P<key>[A-Z][a-z]*) (?P<value>\w+)'
      key: "{key}s.{value}"
- regexp: 'Annex (\w+)'
"""

    parser = text2py.Parser(yaml.load(template, yaml.SafeLoader))
    output = parser.parse(io.StringIO(text))
    assert output == {'A': {'color': 'red', 'number': 'first',
                            'Chapters': {
                                'A1': {},
                                'A2': {}
                            }},
                      'B': {'number': 'second',
                            'Chapters': {
                                'B1': {},
                                'B2': {},
                                'B3': {}
                            }},
                      'C': {'color': 'yellow',
                            'Chapters': {
                                'C1': {}
                            }},}

def test_parser_5():
    template = """
- regexp: 'Volume (?P<volume>\w+)'
  key: "{volume}"
  value:
    - regexp: '(?P<key>[a-z]+) (?P<value>\w+)'
      key: "{key}"
      value: "{value}"
    - regexp: '(?P<key>[A-Z][a-z]*) (?P<value>\w+)( color (?P<color>\w+))?'
      key: "{key}s.{value}"
      value:
        - key: "color"
          value: "{color}"
- regexp: 'Annex (\w+)'
"""

    parser = text2py.Parser(yaml.load(template, yaml.SafeLoader))
    output = parser.parse(io.StringIO(text))
    assert output == {'A': {'color': 'red', 'number': 'first',
                            'Chapters': {
                                'A1': {},
                                'A2': {}
                            }},
                      'B': {'number': 'second',
                            'Chapters': {
                                'B1': {'color': 'red'},
                                'B2': {'color': 'orange'},
                                'B3': {'color': 'yellow'}
                            }},
                      'C': {'color': 'yellow',
                            'Chapters': {
                                'C1': {}
                            }},}
